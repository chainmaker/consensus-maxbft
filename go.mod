module chainmaker.org/chainmaker/consensus-maxbft/v2

go 1.15

require (
	chainmaker.org/chainmaker/chainconf/v2 v2.3.4
	chainmaker.org/chainmaker/common/v2 v2.3.4
	chainmaker.org/chainmaker/consensus-utils/v2 v2.3.5
	chainmaker.org/chainmaker/localconf/v2 v2.3.4
	chainmaker.org/chainmaker/lws v1.2.1
	chainmaker.org/chainmaker/pb-go/v2 v2.3.5
	chainmaker.org/chainmaker/protocol/v2 v2.3.5
	chainmaker.org/chainmaker/utils/v2 v2.3.5
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.6.0
	github.com/golang/protobuf v1.5.2
	github.com/panjf2000/ants/v2 v2.4.8
	github.com/stretchr/testify v1.8.0
	github.com/tmthrgd/go-hex v0.0.0-20190904060850-447a3041c3bc
	go.uber.org/atomic v1.7.0
)

replace (
	github.com/libp2p/go-libp2p-core => chainmaker.org/chainmaker/libp2p-core v1.0.0
	github.com/linvon/cuckoo-filter => chainmaker.org/third_party/cuckoo-filter v1.0.0
	google.golang.org/grpc => google.golang.org/grpc v1.26.0
)
